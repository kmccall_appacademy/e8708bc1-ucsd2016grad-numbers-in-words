ONES = {
  0 => "zero",
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS = {
  10 => "ten",
  11 => "eleven",
  12 => "twelve",
  13 => "thirteen",
  14 => "fourteen",
  15 => "fifteen",
  16 => "sixteen",
  17 => "seventeen",
  18 => "eighteen",
  19 => "nineteen"
}

TENS = {
  20 => "twenty",
  30 => "thirty",
  40 => "forty",
  50 => "fifty",
  60 => "sixty",
  70 => "seventy",
  80 => "eighty",
  90 => "ninety"
}

MAGNITUDES = {
  100 => "hundred",
  1000 => "thousand",
  1_000_000 => "million",
  1_000_000_000 => "billion",
  1_000_000_000_000 => "trillion"
}

class Fixnum
  def in_words
    words = ""
    if self < 10
      words << ONES[self]
    elsif self < 20
      words << TEENS[self]
    elsif self < 100
      tens = (self / 10) * 10
      one = self % tens
      words << TENS[tens]
      if (one != 0)
        words << " "
        words << ONES[one]
      end
    else
      magnitude = set_magnitude
      words << (self/magnitude).in_words
      words << " "
      words << MAGNITUDES[magnitude]
      remaind = self % magnitude
      if remaind != 0
        words << " "
        words << remaind.in_words
      end
    end
    words
  end

  def set_magnitude
    magns = MAGNITUDES.keys.select {|k|  k <= self}
    magns.last
  end

end
